//Collections
import '/imports/api/teams/_teams.js';
import '/imports/api/players/_players.js';
import '/imports/api/settings/_settings.js';

// Fixtures
import '/imports/api/users/fixtures.js';

//Publish
import '/imports/api/users/publish.js';

//Methodes
import '/imports/api/users/methods.js';
import '/imports/api/teams/methods.js';
import '/imports/api/players/methods.js';
import '/imports/api/settings/methods.js';
import React from 'react';
import ReactDOM from 'react-dom';

class Layout extends React.Component {

    constructor() {
        super();
        this.state = {
            access: 'loading'
        }
    }

    componentDidMount () {

    }

    render() {

            return (
                <div className="main-layout">
                    {this.props.content}
                </div>
            )
    }

}

export default Layout;

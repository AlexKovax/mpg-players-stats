import { Settings } from '/imports/api/settings/_settings.js';
import { HTTP } from 'meteor/http';

Meteor.methods({
    getSetting(setting) {
        return Settings.findOne({ _id: setting });
    },
    updateCurrentDay() {
        let currentDaySettings = Settings.findOne({ _id: 'currentDay' });
        let currentDay = 0;

        if (typeof currentDaySettings !== 'undefined') {
            if (currentDaySettings.lastUpdate < (Math.floor(Date.now() / 1000) - 3600) || currentDaySettings.lastUpdate === NaN) {
                currentDay = Meteor.call('getCurrentDay');
                Settings.update({ _id: 'currentDay' }, { $set: { value: currentDay, lastUpdate: Math.floor(Date.now() / 1000) } })
            } else {
                currentDay = currentDaySettings.value;
            }
        } else {
            currentDay = Meteor.call('getCurrentDay');
            Settings.insert({ _id: 'currentDay', value: currentDay, lastUpdate: Math.floor(Date.now() / 1000) })
        }

        return currentDay;
    },
    getCurrentDay() {
        url = 'https://api.monpetitgazon.com/championship/1/calendar/';

        console.log('Getting current day of championship');

        let response = HTTP.get(
            url
        )

        let data = JSON.parse(response.content);

        console.log('Current day is ' + data.day);

        //compute games data
        let dataMatches = { _id: 'dataMatches' };
        data.matches.forEach((match) => {
            dataMatches[match.home.club] = { home: true, quote: match.quotationPreGame.Home, opponent: match.away.club, opponentQuote: match.quotationPreGame.Away };
            dataMatches[match.away.club] = { home: false, quote: match.quotationPreGame.Away, opponent: match.home.club, opponentQuote: match.quotationPreGame.Home };
        });

        Settings.remove({ _id: 'dataMatches' });
        Settings.insert(dataMatches);

        return data.day;
    }
})
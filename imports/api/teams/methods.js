import { HTTP } from 'meteor/http';
import { Teams } from '/imports/api/teams/_teams.js';
import { Players } from '/imports/api/players/_players.js';

Meteor.methods({
  syncTeams(league) {
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzUyMTgwMiIsImNoZWNrIjoiYTkyNGYxMTZmMTg2NTE3MyIsImlhdCI6MTUzMzM5NDU2MX0.5tls3-HtCLlAxIQr9xDEsrP3pyQerca0Sc1Hfjk_WNw'
    url = 'https://api.monpetitgazon.com/league/' + league + '/teams'

    console.log('Getting teams for league : ' + league);
    let teams = [];

    let response = HTTP.get(
      url,
      { headers: { 'Authorization': token } }
    )

    let data = JSON.parse(response.content);

    teamsId = data.teamsid;

    Teams.remove({});

    teamsId.forEach((team) => {
      let teamData = data.teams[team.id];
      teamData._id = team.id;
      teamData.league = league;
      Teams.insert(teamData);
    })

    console.log('Teams synced: ' + Teams.find().count(), teamsId);

    return teams;
  },
  getTeams(league) {
    return Teams.find({ league: { $eq: league } }).fetch();
  },
  getTeam(id) {
    let team = Teams.findOne({ _id: id });
    let newplayers = team.players.map((player) => {
      let fullplayer = Players.findOne({ _id: player.id });
      return { ...player, ...fullplayer };
    })

    team.players = newplayers.sort((a, b) => { return (a.position < b.position) ? -1 : 1 });

    return team;
  },
  recalcTeamsStats(idTeam) {
    //TODO
    //parcourir tous les joueurs de la base
    //-> note moyenne de l'équipe
    //-> somme des buts

    //Pour l'instant on on update juste les stats des joueurs de la team en cours
    let team = Meteor.call('getTeam', idTeam);

    //boucle all players
    team.players.forEach((player) => {
      Meteor.call('fullUpdatePlayer', player.id);
    })
  }
})

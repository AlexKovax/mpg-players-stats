if ( Meteor.users.find().count() === 0 ) {

    /////////////////////////////////
    // ADMIN
    console.log('Création de l\'admin');
    const admin = {
        username: 'admin',
        email: 'admin@admin.com',
        password: 'admin',
        roles: ['admin'],
        profile: {
            nom: 'Admin',
            prenom: 'Admin'
        }
    };

    let id = Accounts.createUser({
        username: admin.username,
        email: admin.email,
        password: admin.password,
        profile: admin.profile
    });

    if (id) {
        Roles.addUsersToRoles(id, admin.roles);
    }

    ////////////////////////////////////////////////////////
    // USER 1

    console.log('Création d\'un user');

    const user = {
        username: 'user',
        email: 'user@user.com',
        password: 'user',
        roles: ['user'],
        profile: {
            nom: 'user',
            prenom: 'user'
        }
    }

    let id2 = Accounts.createUser({
        username: user.username,
        email: user.email,
        password: user.password,
        profile: user.profile
    });
    if (id2) {
        Roles.addUsersToRoles(id2, user.roles);
    }

}

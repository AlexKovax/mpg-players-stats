import { Players } from '/imports/api/players/_players.js';
import { HTTP } from 'meteor/http';

Meteor.methods({
    initPlayers() {
        //https://api.monpetitgazon.com/stats/championship/1

        token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzUyMTgwMiIsImNoZWNrIjoiYTkyNGYxMTZmMTg2NTE3MyIsImlhdCI6MTUzMzM5NDU2MX0.5tls3-HtCLlAxIQr9xDEsrP3pyQerca0Sc1Hfjk_WNw'
        url = 'https://api.monpetitgazon.com/stats/championship/1'

        console.log('Getting players');
        let players = [];

        let response = HTTP.get(
            url
            /*{headers: {'Authorization': token}}*/
        )

        players = JSON.parse(response.content);

        Players.remove({});

        players.forEach((player) => {
            player._id = player.id;
            player.createdAt = new Date();
            Players.insert(player);
        })

        console.log('Players synced: ' + Players.find().count());

        return players;
    },
    refreshBasePlayersStats() {
        token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzUyMTgwMiIsImNoZWNrIjoiYTkyNGYxMTZmMTg2NTE3MyIsImlhdCI6MTUzMzM5NDU2MX0.5tls3-HtCLlAxIQr9xDEsrP3pyQerca0Sc1Hfjk_WNw'
        url = 'https://api.monpetitgazon.com/stats/championship/1'

        console.log('Refreshing basic players stats');
        let players = [];

        let response = HTTP.get(
            url
            /*{headers: {'Authorization': token}}*/
        )

        players = JSON.parse(response.content);

        players.forEach((player) => {
            Players.update({ _id: player.id }, { $set: { quotation: player.quotation, stats: player.stats } });
        })

        console.log('Players updated: ' + Players.find().count());

        return players;
    },
    fullUpdatePlayer(idPlayer) {
        //récupérer les stats d'un joueur
        //https://api.monpetitgazon.com/stats/player/61278?season=2018
        //récupérer une seule fois pour la saison précédente

        let id = idPlayer.replace('player_', '');
        url = 'https://api.monpetitgazon.com/stats/player/' + id + '?season=2018'

        console.log('Getting full stats for player ' + id);

        let response = HTTP.get(
            url
            /*{headers: {'Authorization': token}}*/
        )

        let data = JSON.parse(response.content);

        //Calcul divers
        const reducer = (accumulator, currentValue) => { return accumulator + currentValue.info.rate };
        let last3Avg = (data.stats.matches.length > 0) ? Math.round((data.stats.matches.slice(0, 3).reduce(reducer, 0) / 3) * 100) / 100 : 0;

        Players.update({ _id: idPlayer }, { $set: { fullStats: data.stats, last3Avg, lastFullUpdate: new Date() } });

        let player = Players.findOne({ _id: idPlayer });

        //moyenne 3 dernieres saison
        if (typeof player.lastYearAvg === 'undefined') {
            Meteor.call('getLastSeasonAvg', idPlayer);
        }

        return data.stats;
    },
    getLastSeasonAvg(idPlayer) {
        let id = idPlayer.replace('player_', '');
        url = 'https://api.monpetitgazon.com/stats/player/' + id + '?season=2017'

        console.log('Getting last year stats for player ' + id);

        let response = HTTP.get(
            url
            /*{headers: {'Authorization': token}}*/
        )

        let data = JSON.parse(response.content);

        return Players.update({ _id: idPlayer }, { $set: { lastYearAvg: data.stats.avgRate } });
    }
})
import React from 'react';
import {mount} from 'react-mounter';

import Layout from '/imports/layouts/Layout.jsx';
import Accueil from '/imports/ui/pages/Accueil.jsx';

FlowRouter.route('/', {
    name: 'accueil',
    action: function() {
        mount(Layout, {content: <Accueil />});
    }
});

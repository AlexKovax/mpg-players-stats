import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Button, Icon, Dropdown, Form, List, Label, Divider, Header, Segment, Table } from 'semantic-ui-react';

class Team extends React.Component {

    constructor() {
        super();
        this.state = {
            team: {},
            dataMatches: {}
        };
    }

    componentWillMount() {
        Meteor.call('getSetting', 'dataMatches', (err, res) => {
            if (err) {
                console.log(err)
            } else {
                this.setState({ dataMatches: res })
            }
        })
    }

    componentDidMount() {
        this.getTeam(this.props.teamId);
    }

    getTeam(id) {
        Meteor.call('getTeam', id, (err, res) => {
            if (err) {
                console.log(err);
            } else {
                this.setState({ team: res });
            }
        })
    }

    render() {

        if (typeof this.state.team._id === 'undefined') {
            return (<div>Loading...</div>)
        }

        return (
            <Segment>
                <h2>{this.state.team.name}</h2>

                <Table celled>

                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nom</Table.HeaderCell>
                            <Table.HeaderCell>Position</Table.HeaderCell>
                            <Table.HeaderCell>Equipe</Table.HeaderCell>
                            <Table.HeaderCell>Côte</Table.HeaderCell>
                            <Table.HeaderCell>Buts</Table.HeaderCell>
                            <Table.HeaderCell>Nb matchs</Table.HeaderCell>
                            <Table.HeaderCell>Starter %</Table.HeaderCell>
                            <Table.HeaderCell>Moyenne saison passée</Table.HeaderCell>
                            <Table.HeaderCell>Note moyenne</Table.HeaderCell>
                            <Table.HeaderCell>Moyenne 3 derniers</Table.HeaderCell>
                            <Table.HeaderCell>Next game</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.team.players.map((player, i) => {
                            if (typeof player.fullStats === 'undefined') {
                                return (
                                    <Table.Row key={i}>
                                        <Table.Cell>{player.lastname} {player.firstname}</Table.Cell>
                                    </Table.Row>
                                )
                            }

                            //get match data
                            let dataTeam = this.state.dataMatches[player.club];

                            return (
                                <Table.Row key={i}>
                                    <Table.Cell>{player.lastname} {player.firstname}</Table.Cell>
                                    <Table.Cell>{['gardien', 'défenseur', 'milieu', 'attaquant'][player.position - 1]}</Table.Cell>
                                    <Table.Cell>{player.club}</Table.Cell>
                                    <Table.Cell>{player.quotation}</Table.Cell>
                                    <Table.Cell>{player.stats.sumGoals}</Table.Cell>
                                    <Table.Cell>{player.fullStats.matches.length}</Table.Cell>
                                    <Table.Cell>{player.stats.percentageStarter}</Table.Cell>
                                    <Table.Cell>{player.lastYearAvg}</Table.Cell>
                                    <Table.Cell>{player.stats.avgRate}</Table.Cell>
                                    <Table.Cell>{player.last3Avg}</Table.Cell>
                                    <Table.Cell>
                                        {(dataTeam.home) ? 'reçoit' : 'se déplace à'} {dataTeam.opponent} et est donné&nbsp;
                                        {(dataTeam.quote < dataTeam.opponentQuote) ? <b>vainqueur</b> : 'perdant'} à&nbsp;
                                        {dataTeam.quote} contre {dataTeam.opponentQuote}
                                    </Table.Cell>
                                </Table.Row>
                            )
                        })}

                    </Table.Body>
                </Table>
            </Segment>
        )
    }

}

export default Team;

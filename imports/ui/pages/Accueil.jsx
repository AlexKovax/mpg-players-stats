import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Button, Icon, Dropdown, Form, List, Label, Divider, Header, Segment } from 'semantic-ui-react';
import Team from '/imports/ui/components/Team.jsx';

class Accueil extends React.Component {

    constructor() {
        super();
        this.state = {
            teams: [],
            league: 'KJN8L9P2',
            selectedTeam: 'mpg_team_KJN8L9P2$$mpg_user_521802',
            currentDay: 0
        };
    }

    componentWillMount() {
        Meteor.call('updateCurrentDay', (err, res) => {
            if (err) {
                console.log(err)
            } else {
                this.setState({ currentDay: res })
            }
        })
    }

    componentDidMount() {
        this.getTeams();
    }

    getTeams() {
        Meteor.call('getTeams', this.state.league, (err, res) => {
            if (err) {
                console.log(err);
            } else {
                this.setState({ teams: res });
            }
        })
    }

    syncTeams() {
        console.log('Syncing teams!')
        Meteor.call('syncTeams', this.state.league, (err, res) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Teams synced!')
                console.log(res);
            }
        })
    }

    initPlayers() {
        console.log('Initializing players!')
        Meteor.call('initPlayers', (err, res) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Players initialized!')
                console.log(res);
            }
        })
    }

    recalcTeamsStats() {
        console.log('Recalculating teams stats based on players stats')
        Meteor.call('recalcTeamsStats', this.state.selectedTeam, (err, res) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Teams stats recalculated')
                console.log(res);
            }
        })
    }

    refreshBasePlayersStats() {
        console.log('Refreshing basic stats')
        Meteor.call('refreshBasePlayersStats', (err, res) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Basic stats updated')
                console.log(res);
            }
        })
    }

    render() {

        return (
            <Container>
                <h1>MPG Helper</h1>

                <Segment>
                    <h2>Actions</h2>
                    <Button onClick={this.syncTeams.bind(this)}>Sync Teams</Button>
                    <Button onClick={this.initPlayers.bind(this)}>Init players</Button>
                    <Button onClick={this.recalcTeamsStats.bind(this)}>Recalculate teams stats</Button>
                    <Button onClick={this.refreshBasePlayersStats.bind(this)}>Refresh basic stats</Button>
                </Segment>

                <Segment>
                    Journée en cours : {this.state.currentDay}
                </Segment>

                <Segment>
                    <h2>Teams</h2>

                    {this.state.teams.map((team) => {
                        return (
                            <div key={team._id}>{team.name}</div>
                        )
                    })}
                </Segment>

                <Team teamId={this.state.selectedTeam} />
            </Container>
        )
    }

}

export default Accueil;
